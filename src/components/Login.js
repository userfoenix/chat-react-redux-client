import React from 'react';
import { connect } from 'react-redux';
import { login } from '../actions';

const mapStateToProps = state => ({
	corr: state.active_corr,
	me: state.me
});

const mapDispatchToProps = dispatch => {
	return {
		login: () => {
			dispatch(login());
		}
	}
};

const Login = React.createClass({
	componentDidMount: function(){
		window.deviceId = this.props.params.deviceId;
		this.props.login();
	},
	render: function() {
		return (
			<div className="login">
				<h2>Authorization...</h2>
			</div>
		);
	}
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Login);