import React from 'react';
import {setActiveCorr} from '../actions';

import { connect } from 'react-redux';
import { Link } from 'react-router'

const mapStateToProps = (state, props) => {
	return {};
};
const mapDispatchToProps = (dispatch, ownProps) => {
	return {};
};

class ContactItem extends React.Component {
	constructor(props) {
		super(props);
	}
	render (){
		var { corr, name, unread, is_active, deviceId, avatar} = this.props;
		var path = `/device/${deviceId}/corr/${corr}`;
		return (
			<li className={"left clearfix contact-item"+(is_active ? ' active' : '')}>
				<Link to={path}>
					{ name && <span className="chat-img pull-left"><img src={avatar} alt={name} className="rounded-circle"/></span> }
					<div className="chat-body clearfix">
						<div className="header">
							<strong className="primary-font">{name}</strong>
							{unread>0 && <span className="badge counter-unread" title="unread messages">{unread}</span>}
						</div>
					</div>
				</Link>
			</li>
		);
	}
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ContactItem)