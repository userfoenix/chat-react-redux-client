import React from 'react';
import { Link } from 'react-router';
import { makeId } from '../utils/utils';

class Devices extends React.Component {
	constructor(props) {
		super(props);
		var devices = [];
		if (window.localStorage) {
			for (let key in localStorage) {
				let m = key.match(/state_(.*)/);
				if (m && m[1]) {
					devices.push(m[1]);
				}
			}
		}
		this.state = {devices};
	}
	removeDevice(device){
		var res = true; //confirm("Are you sure?");
		if (res) {
			localStorage.removeItem('state_'+device);
			this.setState((prevState) => ({
				devices: prevState.devices.filter(d => d!==device)
			}));
		}
	}
	render() {
		var path = '/device/'+makeId(7);
		var devices = this.state.devices;
		return (
			<div className="container">
				<h2>Select device:</h2>
				<table className="table table-striped">
					<tbody>
						<tr className="active">
							<th>Device Id</th>
							<th>Open</th>
							<th>Remove</th>
						</tr>
						{
							devices.map(device => {
								let path = `/device/${device}`;
								return <tr key={path}>
									<td>{device}</td>
									<td><Link to={path}>Open</Link></td>
									<td><a className="link" onClick={this.removeDevice.bind(this,device)}>Remove</a></td>
								</tr>;
							})
						}
					</tbody>
					<tfoot>
						<tr key="new" className="success">
							<td><Link to={path}>Register new device</Link></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</tfoot>
				</table>
			</div>
		);
	}
}

export default Devices;