import React from 'react';
import ContactList from './ContactList';
import MessagesList from './MessagesList';
import TextBox from './TextBox';
//import DevTools from './DevTools';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router'
import { initialState, logout, loggedOut, restoreState } from '../actions';

const mapStateToProps = ({me,active_corr,session}) => ({me,active_corr,session});
const mapDispatchToProps = dispatch => {
	return {
		logout: () => {
			console.info('Logout...');
			dispatch(logout());
			dispatch(restoreState(initialState));
			dispatch(loggedOut());
			browserHistory.push(`/`);
		}
	}
};

class App extends React.Component {
	constructor(props) {
		super(props);
		this.logout = this.props.logout.bind(this);
	}
	componentDidMount() {
		const w = window,
			d = document,
			documentElement = d.documentElement,
			body = d.getElementsByTagName('body')[0],
			width = w.innerWidth || documentElement.clientWidth || body.clientWidth,
			height = w.innerHeight|| documentElement.clientHeight|| body.clientHeight;
		console.log("height,width", height,width);
		this.refs['panel-body'].style.height = (height-122)+'px';
		this.refs['panel-contacts'].style.height = (height-50)+'px';
		console.log("this.refs['panel-body']", this.refs['panel-body']);
		console.log("this.refs['panel-contacts']", this.refs['panel-contacts']);
	}
	render() {
		var deviceId = this.props.params.deviceId;
		return (
<div className="container">
	<div className="row">
		<div className="col-md-3 nopadding">
			<div className="panel panel-primary panel-contacts">
				<div className="panel-heading" id="accordion">
					<span className="glyphicon glyphicon-user"></span>
					<span className="panel-title">Contacts</span>
				</div>
				<div className="panel-collapse" id="collapseOne">
					<div className="panel-body panel-contacts" ref="panel-contacts" id="contacts">
						{ this.props.session.processing ? 'Loading...' : <ContactList deviceId={deviceId}/> }
					</div>
				</div>
			</div>
		</div>
		<div className="col-md-9 hidden-xs nopadding">
			<div className="panel panel-primary panel-messages">
				<div className="panel-heading" id="accordion">
					<span className="glyphicon glyphicon-comment"></span>
					<span className="panel-title">Messages</span>
					<span className="user-info">
						<a onClick={this.logout}>Logout:&nbsp;{this.props.me.full_name}</a>
					</span>
				</div>
				<div className="panel-collapse" id="collapseOne">
					<div className="panel-body" ref="panel-body" id="messages">
						{ this.props.children }
					</div>
					<div className="panel-footer">
						<TextBox/>
					</div>
				</div>
			</div>
		</div>
	</div>
	{/* <DevTools/> */}
</div>);
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(App);