import React from 'react';
import { connect } from 'react-redux';
import { logout, loggedOut } from '../actions';

const mapStateToProps = state => ({
	corr: state.active_corr,
	me: state.me
});

const mapDispatchToProps = dispatch => {
	return {
		logout: () => {
			dispatch(logout());
		}
	}
};

const Logout = React.createClass({
	componentDidMount: function(){
		this.props.logout();
	},
	render: function() {
		return (
			<div className="login">
				<h2>Authorization...</h2>
			</div>
		);
	}
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Logout);