import React from 'react';
import ContactItem from './ContactItem';

import { connect } from 'react-redux';

const mapStateToProps = ({contacts,active_corr, unread}) => {
	return {
		contacts: contacts.list.map(c => {
			return Object.assign({},c,{unread:unread[c.user_id] ? unread[c.user_id].length : 0})
		}),
		active_corr
	}
};

const ContactList = React.createClass({
	handleClick: function(i, props){
		this.setState({active: props.id});
	},
	render: function() {
		//console.log("this.props", this.props);
		return (
<ul className="chat">
	{
		this.props.contacts.map((contact,i) => {
			return <ContactItem
				key={contact.user_id}
				deviceId={this.props.deviceId}
				corr={contact.user_id}
				name={contact.full_name}
				unread={contact.unread}
				avatar={contact.avatar}
				is_active={this.props.active_corr==contact.user_id? 1 : 0}
			/>;
		})
	}
</ul>);
	}
});

export default connect(
	mapStateToProps
)(ContactList);
