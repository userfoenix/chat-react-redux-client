import React from 'react';
import { connect } from 'react-redux';
import { login } from '../actions';

const mapStateToProps = state => ({
	corr: state.active_corr,
	me: state.me
});

const mapDispatchToProps = dispatch => {
	return {};
};

const ChooseCorrespondent = React.createClass({
	componentDidMount: function(){
		//console.log("this", this);
		//window.deviceId = this.props.params.deviceId;
	},
	render: function() {
		return (
			<div className="messages-notify">
				<span>Choose correspondent to talk</span>
			</div>
		);
	}
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ChooseCorrespondent);