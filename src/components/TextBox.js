import React from 'react';
import { connect } from 'react-redux'
import { sendMessage } from '../actions'

const mapStateToProps = state => ({
	corr: state.active_corr,
	me: state.me
});

const mapDispatchToProps = dispatch => {
	return {
		onSubmit: (corr,text,file) => {
			dispatch(sendMessage({corr,text,file}));
		}
	}
};

class TextBox extends React.Component {
	constructor(props) {
		super(props);
		this.onSubmit = this.onSubmit.bind(this);
	}
	onSubmit(e){
		e.preventDefault();
		var corr = ''+this.props.corr;
		var text = this.text.value;
		var file = this.attach.files[0];
		this.props.onSubmit(corr,text,file);
		this.form.reset();
	}
	componentDidMount(){
		this.text.focus();
	}
	componentDidUpdate(){
		this.text.focus();
	}
	render(){
		return (
			<form
				onSubmit={this.onSubmit}
				ref={(form) => { this.form = form; }} >
			<div className="input-group">
				<input
					autoComplete="off"
					id="btn-input"
					type="text"
					ref="text"
					className="form-control input-sm"
					placeholder='Type your message here...'
					ref={(input) => { this.text = input; }} />
				<span className="input-group-btn">
					<button className="btn btn-warning btn-sm" id="btn-chat">Send</button>
				</span>
			</div>
			<div className="input-group">
				<input
					type="file"
					style={{paddingTop:'5px'}}
					ref={(input) => { this.attach = input; }} />
			</div>
			</form>
		);
	}
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(TextBox);
