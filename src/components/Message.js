import React from 'react';
import moment from 'moment';

const Message = React.createClass({
	render: function() {
		const {name,text,tm,avatar,outgoing,unread} = this.props;
		var addclass = '';
		if (outgoing){ addclass += ' outgoing'; }
		if (unread){ addclass += ' unread'; }
		return (
<li className={"left clearfix"+addclass}>
	<span className="chat-img pull-left">
		<img src={avatar} alt="User Avatar" className="rounded-circle" />
	</span>
	<div className="chat-body clearfix">
		<div className="header">
			<strong className="primary-font">{name}</strong>
			<small className="pull-right text-muted">{moment(tm*1000).format('DD MMM YY, H:s')}</small>
		</div>
		<div className="text">{text}</div>
	</div>
</li>
		);
	}
});

export default Message;