import React from 'react';
import Message from './Message';
import { connect } from 'react-redux';

const mapStateToProps = ({messages,contacts,active_corr,last_read,me}) => {
	var corr_user = contacts.list.filter(c => c.user_id==active_corr)[0] || {};
	var corr_name = corr_user.full_name || active_corr;
	var cur_last_read = last_read[active_corr] || '';
	var messages_list = messages.list.filter(o =>
			o.corr==active_corr
		).map(o => {
			const own = o.user_id==me.user_id;
			return Object.assign({}, o, {
				is_unread: 0,
				is_outgoing: 0,
				own: own,
				name: own ? me.full_name : corr_name,
				avatar: own ? me.face_photo_url : corr_user.avatar
			});
		});
	var last_is_own = messages_list.length ? messages_list[messages_list.length-1].own : false;
	var has_unread = false;
	if (cur_last_read){
		for (var i=messages_list.length-1,changed=false; i>=0; i--){
			if (messages_list[i].own!=last_is_own){
				changed = true;
			}
			if (!changed && messages_list[i].msg_id>cur_last_read){
				if (!last_is_own){
					messages_list[i].is_unread = 1;
				}
				else{
					messages_list[i].is_outgoing = 1;
				}
			}
			else{
				messages_list[i].is_unread = 0;
				messages_list[i].is_outgoing = 0;
			}
		}
	}
	return {
		last_read: cur_last_read,
		messages: messages_list || [],
		active_corr
	}
};

class MessagesList extends React.Component {
	constructor(props) {
		super(props);
	}
	scrollToBottom(){
		var container = this.refs['msg-list'].parentNode;
		if (container){
			container.scrollTop = container.scrollHeight;
		}
	}
	componentDidMount(){
		this.scrollToBottom();
	}
	componentDidUpdate(){
		this.scrollToBottom();
	}
	render(){
		var {messages,last_read} = this.props;
		//console.groupCollapsed('Render messages: '+Date.now());
		//console.log("last_read", last_read);
		//console.table(messages);
		//console.groupEnd();
		//var last_unread;
		return (
<ul className="chat" ref="msg-list">
	{
		messages.map(({msg_id,name,text,timestamp,is_unread,is_outgoing,avatar}) => {
			if (!msg_id){
				console.warn({msg_id,name,text,timestamp,is_unread,is_outgoing,avatar});
			}
			return <Message key={msg_id} name={name} text={text} tm={timestamp} outgoing={is_outgoing} unread={is_unread} avatar={avatar}/>;
		})
	}
</ul>);
	}
}

export default connect(
	mapStateToProps
)(MessagesList);
