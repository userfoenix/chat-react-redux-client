import io from 'socket.io-client';
import * as actions from './actions';
import Config from './config';
import { browserHistory } from 'react-router';
import APIClass from './utils/api-manager';
import moment from 'moment';

let socket = null;

function getLocation(href) {
	var match = href.match(/^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)([\/]{0,1}[^?#]*)(\?[^#]*|)(#.*|)$/);
	return match && {
		protocol: match[1],
		host: match[2],
		hostname: match[3],
		port: match[4],
		pathname: match[5],
		search: match[6],
		hash: match[7]
	}
}

const sessionExpired = function(data){
	console.info('Session expired:', moment().format('DD MMM YY, H:mm:ss'));
	const {pathname} = browserHistory.getCurrentLocation();
	browserHistory.push(pathname);
	store.dispatch(actions.loggedOut());
	store.dispatch(actions.login());
};

export const initApiServer = (store) => {
	const {session,messages,active_corr} = store.getState();
	if (!session || !session.data){
		console.log('Invalid session',session);
		return;
	}

	const modtime = messages && messages.modtime ? messages.modtime : 0;
	const {debug,access_token,user_id,resource_uri,api_connection_uri} = session.data;
	const {version} = Config.api_server;
	const Owner = user_id;

	let api_uri = Config.debug ? api_connection_uri.replace('https','http') : api_connection_uri;

	var url = `${api_uri}?user_id=${user_id}&token=${access_token}`;

	// Parse resource
	var parts = getLocation(resource_uri);
	if (parts && parts.hostname){
		url += '&resource_uri='+parts.hostname;
	}
	// --

	// Set api version
	if (typeof(version)!='undefined'){
		url += '&v='+version;
	}

	console.log("socket.io connection url", url);
	socket = io.connect(url);

	const API = new APIClass(socket, {debug:0});

	socket.on('update', (result) => {
		console.info('onUpdate',result);
		store.dispatch(actions.saveEntities(result));
	});

	socket.on('connect', () => {
		console.log("Client `%s` connected!", user_id);
		API.init({messages:modtime}, (error,data) => {
			console.log("onInit", error, data);
			if (data){
				store.dispatch(actions.saveEntities(data));
			}
		});
	});

	// Received session expired notify
	socket.on('session.expired', data => {
		console.log("session.expired", data);
		sessionExpired();
	});

	// Received inbox message
	socket.on('messages.msg', data => {
		if (data.req_id) return; // Sync command was handled inside APIClaas
		console.log("msg received", JSON.stringify(data));
		let {msg_id,user_id,text,modtime} = data;
		let cmd = {
			corr: ''+user_id,
			timestamp: modtime,
			msg_id: ''+msg_id,
			text
		};
		console.log("cmd", cmd);
		store.dispatch(actions.addMessage(cmd));
	});

	// Received outgoing message
	socket.on('messages.msg.outgoing', data => {
		if (data.req_id) return; // Sync command was handled inside APIClaas
		console.log("outgoing msg received", user_id, JSON.stringify(data));
		let {msg_id,user_id,text,modtime} = data;
		let cmd = {
			corr: ''+user_id,
			user_id: ''+Owner,
			timestamp: modtime,
			msg_id: ''+msg_id,
			text
		};
		console.log("cmd", cmd);
		store.dispatch(actions.addMessage(cmd));
	});

	// todo: implement edit msg
	socket.on('messages.msg.edit', data => {
		console.log('messages.msg.edit',data);
	});

	// todo: implement delete msg
	socket.on('messages.msg.delete', data => {
		console.log('messages.msg.delete',data);
	});

	// Received contact
	socket.on('messages.contact', data => {
		if (data.req_id) return; // Sync command was handled inside APIClaas
		console.log("contact received", JSON.stringify(data));
		store.dispatch(actions.addContact(data));
	});

	return API;
};