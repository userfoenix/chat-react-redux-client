const Config = {
	debug: true,
	auth_wnd: {
		width: 400,
		height: 450,
		left: 100,
		top: 150,
		resizable: 'yes',
		scrollbars: 'yes',
		status: 'yes'
	},
	dev_auth_endpoint: 'http://app.unet.ne/auth',
	prod_auth_endpoint: 'https://app.unet.com/auth',
	api_server: {
		version: 1
	}
};

export const getAuthEndPoint = () => {
	return Config.debug ? Config.dev_auth_endpoint : Config.prod_auth_endpoint;
};

export const getWndOpts = () => {
	var result = '';
	const wnd_props = ['width','height','resizable','scrollbars','status','left','top'];
	for (let opt in Config.auth_wnd){
		if (wnd_props.indexOf(opt)>=0){
			result += `${opt}=${Config.auth_wnd[opt]},`;
		}
	}
	return result.slice(0,-1);
};

export default Config;