import {ADD_CONTACT,ADD_CONTACTS,SET_CONTACT} from '../actions';

const contact = (state = {}, action) => {
	// Get avatars from real server
	if (action.data.avatar){
		action.data.avatar = action.data.avatar.replace('unet.ne','unet.net');
	}
	// --
	switch (action.type) {
		case ADD_CONTACT:
			return Object.assign({}, action.data);
		case SET_CONTACT:
			if (state.corr !== action.corr) {
				return state
			}
			return Object.assign({}, state, action.data);
		default:
			return state
	}
};

const findById = (user_id,state) => {
	return state.list.some(msg => msg.user_id==user_id);
};

const contacts = (state = [], action) => {
	switch (action.type) {
		case ADD_CONTACT:
			if (findById(action.data.user_id, state)){
				// Prevent insert duplicate
				// todo: update item instead
				return state;
			}
			return Object.assign({},state,{
				list: [].concat(state.list, contact(undefined,action))
			});
	case ADD_CONTACTS:
		return Object.assign({},state,{
			list: [].concat(state.list, action.list.filter(new_contact => {
					return !state.list.some(c => c.user_id==new_contact.user_id)
				})
				.map(c =>
					Object.assign({}, c, {avatar: c.avatar ? c.avatar.replace('unet.ne','unet.net') : ''})
				)
			)
		});
		case SET_CONTACT:
			return state.list.map(t => contact(t, action));
		default:
			return state
	}
};

export default contacts