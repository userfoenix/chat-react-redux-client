import { ADD_MESSAGE, ADD_MESSAGES, ON_SENT_MESSAGE, GET_HISTORY } from '../actions';

const message = (state = {}, action) => {
	switch (action.type) {
	case ADD_MESSAGE:
		return Object.assign({}, action.data);
	case ON_SENT_MESSAGE:
		if (state.msg_id !== action.prev_id) {
			return state;
		}
		return Object.assign({}, state, action.data);
	default:
		return state
	}
};

const findById = (msg_id,state) => {
	return state.list.some(msg => msg.msg_id==msg_id);
};

const messages = (state = [], action) => {
	switch (action.type) {
	case ADD_MESSAGE:
		if (findById(action.data.msg_id, state)){
			// Prevent insert duplicate
			// todo: update item instead
			return state;
		}
		return Object.assign({},state,{
			//modtime: action.modtime > 0 ? action.modtime : state.modtime,
			list: [].concat(state.list, message(undefined,action))
		});
	case ON_SENT_MESSAGE:
		return Object.assign({},state,{
			modtime: action.data.timestamp>state.modtime ? action.data.timestamp : state.modtime,
			list: state.list.map(t => message(t, action))
		});
	case GET_HISTORY:
		return Object.assign({},state,{
			processing: action.processing
		});
	case ADD_MESSAGES:
		//console.log("ADD_MESSAGES", action);
		//var updates = {};
		//action.list.forEach(({msg_id}) => {
		//	updates[msg_id] = findById(msg_id, state);
		//});
		//console.log("updates", updates);
		const state1 = Object.assign(
			{},
			state,
			{
				processing: action.processing,
				modtime: action.modtime>state.modtime ? action.modtime : state.modtime,
				list: [].concat(
					state.list,
					action.list.filter(new_msg =>
						false===findById(new_msg.msg_id,state)
					)
					.map(msg =>
						Object.assign({}, msg)
					)
				).sort((a,b) =>
					a.timestamp-b.timestamp
				)
			}
		);
		return state1;
	default:
		return state
	}
};

export default messages