export const loadState = (guid) => {
	if (!guid){
		console.error('Guid is required!');
		return;
	}
	try {
		var state = localStorage.getItem('state_'+guid);
		if (state!==null){
			//console.log("state from LS", state);
			var res = JSON.parse(state);
			return res;
		}
		return undefined;
	}
	catch (e){
		return undefined;
	}
};

export const saveState = (guid,state,allowed) => {
	if (!guid){
		console.error('Guid is required!');
		return false;
	}
	if (!state.restored){
		console.info('Not restored',state);
		return false;
	}
	try {
		var state_to_save = {};
		allowed.forEach(key => {
			if (state[key]){
				state_to_save[key] = state[key];
			}
		});
		const serializedState = JSON.stringify(state_to_save);
		localStorage.setItem('state_'+guid, serializedState);
		return true;
	}
	catch(e){
		return true;
	}
};

