function request (entity,cmd,{resource_uri,access_token}){
	var url  = `${resource_uri}${entity}/`;
	console.log("request", url, `Bearer ${access_token}`);
	url = url.replace('https','http'); // for test serv
	return fetch(url+'?q='+JSON.stringify(cmd), {
		headers: {
			Authorization: `Bearer ${access_token}`
		}
	}).then(response=>{
		return response.json();
	});
}

export const getUserData = ({resource_uri,access_token}) => {
	var entity = 'user';
	var cmd = {info:["details","birthday","email"]};
	return request(entity, cmd, {resource_uri,access_token});
};

export const sendMsg = ({corr,text,file,attachment_param='image'},session) => {
	// console.log("sendMsg", {corr,text,file});
	let {resource_uri,access_token} = session;
	let entity = 'messages';
	let url  = `${resource_uri}${entity}/`;

	// For development mode (windows started)
	url = url.replace('https','http');

	let payload = {corr, text};
	let formData = new FormData();
	formData.append('access_token',access_token);

	if (file){
		formData.append('image', file);
		payload.attachment_param = 'image';
	}

	formData.append('q', JSON.stringify({msg_send: payload}));

	return fetch(url, {method: "POST", body: formData})
		.then(response => {
			return response.json();
		})
		.then(res => {
			return res && res.msg_send ? res.msg_send : res;
		});
};