const express = require('express');
const path = require('path');
const port = process.env.PORT || 8888;
const host = process.env.HOST || 'chat.unet.pr';
const app = express();

// serve static assets normally
app.use(express.static(__dirname + '/public'));

app.get('*', function (request, response){
	response.sendFile(path.resolve(__dirname, 'public', 'index.html'));
});

app.listen(port, host);
console.log(`Server started on ${host}:${port}`);